# Traffic Timelapse

create timelapse of traffic using google map's historical traffic data
try around city centers and also interstate system
visualization may look like pumping blood through arteries


## Notes

the official google maps api doesn't seem to expose the historical traffic data
so look into using puppeteer to generate the images.
