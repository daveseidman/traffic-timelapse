const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { HotModuleReplacementPlugin } = require('webpack');

module.exports = {
  mode: 'development',
  entry: './client/index.js',
  devtool: 'source-map',
  devServer: {
    hot: true,
    historyApiFallback: true,
    proxy: [
      {
        context: ['/getProducts', '/getOrders', '/getLocations', '/purchase', '/login', '/placeOrder', '/updateProduct', '/deleteProduct', '/updateOrder', '/deleteOrder'], // TODO: just subfolder all api calls
        target: 'http://localhost:8000',
      },
      {
        context: ['/socket.io'],
        target: 'http://localhost:8000',
        ws: true,
      },
    ],
    watchOptions: {
      ignored: [
        path.resolve(__dirname, 'client/assets/images/products'),
      ],
    },
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({ template: 'client/index-template.html' }),
    new CopyWebpackPlugin([{ from: 'client/assets', to: 'assets' }]),
    new HotModuleReplacementPlugin(),
  ],
  module: {
    rules: [{
      test: /\.scss$/,
      use: [
        { loader: 'style-loader' },
        { loader: 'css-loader' },
        { loader: 'sass-loader' },
      ],
    }, {
      test: /\.(png|svg|jpg|gif|ttf|eot|woff|woff2|mp4|mtl|obj|fbx|gltf)$/,
      use: [
        'file-loader',
      ],
    }],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  node: {
    fs: 'empty',
  },
};
